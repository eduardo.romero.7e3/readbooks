package cat.itb.readbooks;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.BookViewHolder> {
    List<BookModel> list;

    public BookAdapter(List<BookModel> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.book_list_item, parent, false);
        return new BookViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull BookViewHolder holder, int position) {
        holder.bindData(list.get(position));
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setBooks(List<BookModel> list){
        this.list = list;
        notifyDataSetChanged();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder{

        TextView title, author, genre, status;
        RatingBar ratingBar;

        public BookViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.titleTextView);
            author = itemView.findViewById(R.id.authorTextView);
            genre = itemView.findViewById(R.id.genreTextView);
            status = itemView.findViewById(R.id.statusTextView);
            ratingBar = itemView.findViewById(R.id.ratingBar);
        }

        @SuppressLint("ResourceAsColor")
        public void bindData(BookModel book){
            title.setText(book.getName());
            author.setText(book.getAuthor());
            genre.setText(book.getGenre());
            ratingBar.setRating(book.getRating());

            switch(book.getStatus()){
                case 0:
                    status.setText("PENDING!");
                    status.setTextColor(Color.GRAY);
                    break;
                case 1:
                    status.setText("READING!");
                    status.setTextColor(Color.GREEN);
                    break;
                case 2:
                    status.setText("READ!");
                    status.setTextColor(Color.CYAN);
                    break;
                default:
                    break;
            }
        }
    }
}
