package cat.itb.readbooks;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

public class AddBookFragment extends Fragment {
    EditText addTitle, addAuthor, addGenre;
    Spinner status;
    RatingBar ratingBar;
    Button addButton;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.add_book_fragment, container, false);

        addTitle = v.findViewById(R.id.addTitleEditText);
        addAuthor = v.findViewById(R.id.addAuthorEditText);
        addGenre = v.findViewById(R.id.addGenreEditText);
        status = v.findViewById(R.id.spinner);
        ratingBar = v.findViewById(R.id.addRatingBar);
        addButton = v.findViewById(R.id.addBookButton);

        status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == 2){
                    ratingBar.setClickable(true);
                    ratingBar.setEnabled(true);
                }
                else {
                    ratingBar.setClickable(false);
                    ratingBar.setEnabled(false);
                    ratingBar.setRating(0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(addTitle.getText().toString().isEmpty() ||
                        addAuthor.getText().toString().isEmpty() ||
                        addGenre.getText().toString().isEmpty()){
                    Toast.makeText(getContext(), "No field must be empty!", Toast.LENGTH_SHORT).show();
                }
                else {
                    BookListFragment fragment = new BookListFragment();
                    Bundle args  = new Bundle();
                    args.putString("title", addTitle.getText().toString());
                    args.putString("author", addAuthor.getText().toString());
                    args.putString("genre", addGenre.getText().toString());
                    args.putInt("status", status.getSelectedItemPosition());
                    args.putFloat("rating", ratingBar.getRating());
                    fragment.setArguments(args);
                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.fragmentActivityContainer, fragment).commit();
                }
            }
        });


        return v;
    }
}
