package cat.itb.readbooks;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    BookViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewModel = new ViewModelProvider(this).get(BookViewModel.class);

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragmentActivityContainer);
        if(fragment == null){
            fragment = new BookListFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.fragmentActivityContainer, fragment).commit();
        }
    }

    public BookViewModel getViewModel(){
        return this.viewModel;
    }
}