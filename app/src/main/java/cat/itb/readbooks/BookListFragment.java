package cat.itb.readbooks;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import static cat.itb.readbooks.BookViewModel.list;

public class BookListFragment extends Fragment {

    Button addButton;
    RecyclerView recyclerView;
    BookViewModel viewModel;
    BookAdapter adapter;
    Bundle args;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(BookViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.book_list_fragment, container, false);


        addButton = v.findViewById(R.id.addButton);
        args = this.getArguments();
        recyclerView = v.findViewById(R.id.recyclerView);
        adapter = new BookAdapter(list);

        if(args != null){
            String title = args.getString("title");
            String author = args.getString("author");
            String genre = args.getString("genre");
            int status = args.getInt("status");
            float rating = args.getFloat("rating");
            BookModel book = new BookModel(title, author, genre, status, rating);
            list.add(book);
        }
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        adapter.setBooks(list);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragmentActivityContainer, new AddBookFragment()).commit();
            }
        });

        return v;

    }
}
