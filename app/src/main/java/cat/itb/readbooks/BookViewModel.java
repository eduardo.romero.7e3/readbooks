package cat.itb.readbooks;

import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

public class BookViewModel extends androidx.lifecycle.ViewModel {
    static ArrayList<BookModel> list = new ArrayList<>();

    public BookViewModel() {
    }

    public void addBook(BookModel book){
        list.add(book);
    }

    public List<BookModel> getBooks(){
        return list;
    }
}
