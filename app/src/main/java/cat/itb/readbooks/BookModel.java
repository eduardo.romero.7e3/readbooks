package cat.itb.readbooks;

public class BookModel {
    private String name;
    private String author;
    private String genre;
    private int status;
    private float rating;

    public BookModel(String name, String author, String genre, int status, float rating) {
        this.name = name;
        this.author = author;
        this.genre = genre;
        this.status = status;
        this.rating = rating;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "BookModel{" +
                "name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", genre='" + genre + '\'' +
                ", status=" + status +
                ", rating=" + rating +
                '}';
    }
}
